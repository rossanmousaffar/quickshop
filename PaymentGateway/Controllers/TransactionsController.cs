﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using PaymentGateway.Models;

namespace PaymentGateway.Controllers
{
    public class TransactionsController : ApiController
    {
        private CkPaymentEntities db = new CkPaymentEntities();

        // GET: api/Transactions
        public IQueryable<Transaction> GetTransactions()
        {
            return db.Transactions;
        }

        // GET: api/Transactions/5
        [ResponseType(typeof(Transaction))]
        public IHttpActionResult GetTransaction(Guid id)
        {
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        // PUT: api/Transactions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTransaction(Guid id, Transaction transaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != transaction.transactionId)
            {
                return BadRequest();
            }

            db.Entry(transaction).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransactionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Transactions
        [ResponseType(typeof(Transaction))]
        public IHttpActionResult PostTransaction(ArrayList transactions)
        {
            Transaction transaction = new Transaction();
            CardDetail cardDetail = new CardDetail();
            CardDetailsController card = new CardDetailsController();

            if (transactions.Count > 0)
            {
                transaction = JsonConvert.DeserializeObject<Transaction>(transactions[0].ToString());
                cardDetail = JsonConvert.DeserializeObject<CardDetail>(transactions[1].ToString());

            }

            var _card = card.GetCardDetail(cardDetail.cardNumber);

            if (_card != null)
            {
                HttpClient client = new HttpClient();
                Uri baseAddress = new Uri("https://localhost:44352//");
                client.BaseAddress = baseAddress;
                ArrayList paramList = new ArrayList();               
                paramList.Add(transaction);
                paramList.Add(_card);
                HttpResponseMessage response = client.PostAsync("api/TransactionsApi", new StringContent(
                new JavaScriptSerializer().Serialize(paramList), Encoding.UTF8, "application/json")).Result;

                if (response.IsSuccessStatusCode)
                {
                    if (!ModelState.IsValid)
                    {
                        return BadRequest(ModelState);
                    }
                    transaction.transactionId = Guid.NewGuid();
                    transaction.Status = "Completed";

                    db.Transactions.Add(transaction);

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        if (TransactionExists(transaction.transactionId))
                        {
                            return Conflict();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return CreatedAtRoute("DefaultApi", new { id = transaction.transactionId }, transaction);
                }
            
            }


            return NotFound();
           
                       
        }

        // DELETE: api/Transactions/5
        [ResponseType(typeof(Transaction))]
        public IHttpActionResult DeleteTransaction(Guid id)
        {
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return NotFound();
            }

            db.Transactions.Remove(transaction);
            db.SaveChanges();

            return Ok(transaction);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TransactionExists(Guid id)
        {
            return db.Transactions.Count(e => e.transactionId == id) > 0;
        }
    }
}