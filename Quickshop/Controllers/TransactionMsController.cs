﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Quickshop.Models;

namespace Quickshop.Controllers
{
    public class TransactionMsController : Controller
    {
        private CkMerchantEntities db = new CkMerchantEntities();

        // GET: TransactionMs
        public ActionResult Index()
        {

            var u = db.AspNetUsers.Where(ur => ur.Email == User.Identity.Name).FirstOrDefault();

            var _t = db.TransactionMs.Where(t => t.UserID == Guid.Parse(u.Id)).ToList();

            return View();
        }

        // GET: TransactionMs/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransactionM transactionM = db.TransactionMs.Find(id);
            if (transactionM == null)
            {
                return HttpNotFound();
            }
            return View(transactionM);
        }

        // GET: TransactionMs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TransactionMs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "transID,UserID,ProductsID,Date")] TransactionM transactionM)
        {
            if (ModelState.IsValid)
            {
                transactionM.transID = Guid.NewGuid();
                db.TransactionMs.Add(transactionM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(transactionM);
        }

        // GET: TransactionMs/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransactionM transactionM = db.TransactionMs.Find(id);
            if (transactionM == null)
            {
                return HttpNotFound();
            }
            return View(transactionM);
        }

        // POST: TransactionMs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "transID,UserID,ProductsID,Date")] TransactionM transactionM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transactionM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(transactionM);
        }

        // GET: TransactionMs/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransactionM transactionM = db.TransactionMs.Find(id);
            if (transactionM == null)
            {
                return HttpNotFound();
            }
            return View(transactionM);
        }

        // POST: TransactionMs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            TransactionM transactionM = db.TransactionMs.Find(id);
            db.TransactionMs.Remove(transactionM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
