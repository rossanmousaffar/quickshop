﻿using Quickshop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Quickshop.Controllers
{
    public class HomeController : Controller
    {
        private CkMerchantEntities db = new CkMerchantEntities();
        List<string> pid = new List<string>();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
   
    }
}