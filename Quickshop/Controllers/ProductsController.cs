﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Microsoft.Ajax.Utilities;
using PaymentGateway.Models;
using Quickshop.Models;

namespace Quickshop.Controllers
{
    public class ProductsController : Controller
    {
        private CkMerchantEntities db = new CkMerchantEntities();

        // GET: Products
        public ActionResult Index()
        {
            return View(db.Products.ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductId,Name,Description,Amount")] Product product)
        {
            if (ModelState.IsValid)
            {
                product.ProductId = Guid.NewGuid();
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(product);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(Guid? id)
        {

            if(!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductId,Name,Description,Amount")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(product);
        }
        [HttpPost]
        public JsonResult Payment(decimal amount,string cardnumber, DateTime expiration, string cvvs, string pID)
        {

            HttpClient client = new HttpClient();
            Uri baseAddress = new Uri("https://localhost:44336/");
            client.BaseAddress = baseAddress;
            ArrayList paramList = new ArrayList();
            Transaction transaction = new Transaction { MerchantID = Guid.Parse("42574452-56bc-4d09-8611-cdd0643ec11a"), UserID = User.Identity.Name, DatePuchased = System.DateTime.Now, Amount = amount };
            CardDetail cardDetail = new CardDetail { Email = User.Identity.Name, cardNumber = cardnumber, cvv = int.Parse(cvvs), dateExpired = expiration };
            paramList.Add(transaction);
            paramList.Add(cardDetail);

            HttpResponseMessage response = client.PostAsync("api/Transactions", new StringContent(
            new JavaScriptSerializer().Serialize(paramList), Encoding.UTF8, "application/json")).Result;

            if (response.IsSuccessStatusCode)
            {
                var u = db.AspNetUsers.Where(ur => ur.Email == User.Identity.Name).FirstOrDefault();
                TransactionM _t = new TransactionM();
                _t.transID = Guid.NewGuid();
                _t.ProductsID = Guid.Parse(pID);
                _t.Date = DateTime.Now;
                _t.UserID = Guid.Parse(u.Id);
                db.TransactionMs.Add(_t);
                db.SaveChanges();

                return Json(new { success = false, responseText = "Payment Successfully done and you will received a mail with details product purchased and delivery time." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, responseText = "Error in Payment!" }, JsonRequestBehavior.AllowGet);
            }


            return Json(response);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
