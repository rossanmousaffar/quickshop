﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Quickshop.Startup))]
namespace Quickshop
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
