﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bank.Models
{
    public class Transactions
    {
        public System.Guid transactionId { get; set; }
        public Nullable<System.Guid> MerchantID { get; set; }
        public string UserID { get; set; }
        public Nullable<System.DateTime> DatePuchased { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string Status { get; set; }
    }
}