﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bank.Models
{
    public class CardDetails
    {
        public string Email { get; set; }
        public string cardNumber { get; set; }
        public int cvv { get; set; }
        public System.DateTime dateCreated { get; set; }
        public System.DateTime dateExpired { get; set; }
    }
}