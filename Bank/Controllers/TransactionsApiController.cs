﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Bank.Models;
using Newtonsoft.Json;

namespace Bank.Controllers
{
    public class TransactionsApiController : ApiController
    {
        private CkBankEntities db = new CkBankEntities();

        // GET: api/TransactionsApi
        public IQueryable<Transaction> GetTransactions()
        {
            return db.Transactions;
        }

        // GET: api/TransactionsApi/5
        [ResponseType(typeof(Transaction))]
        public IHttpActionResult GetTransaction(Guid id)
        {
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return NotFound();
            }

            return Ok(transaction);
        }

        // PUT: api/TransactionsApi/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTransaction(Guid id, Transaction transaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != transaction.transId)
            {
                return BadRequest();
            }

            db.Entry(transaction).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransactionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TransactionsApi
        [ResponseType(typeof(Transaction))]
        public IHttpActionResult PostTransaction(ArrayList transactions)
        {
            Transaction transaction = new Transaction();
            Transactions _transaction = new Transactions();
            CardDetails cardDetail = new CardDetails();
            
            if (transactions.Count > 0)
            {
                _transaction = JsonConvert.DeserializeObject<Transactions>(transactions[0].ToString());
                cardDetail = JsonConvert.DeserializeObject<CardDetails>(transactions[1].ToString());

            }

            Card crd = db.Cards.Where(cr => cr.cardNumber == cardDetail.cardNumber && cr.cvv == cardDetail.cvv).FirstOrDefault();
            
            if (crd.expirationDate >= DateTime.UtcNow)
            {
                Account acc = db.Accounts.Where(a => a.accId == crd.accId).FirstOrDefault();
                decimal? postB = acc.balance;
                if (acc.balance >= _transaction.Amount)
                {
                    acc.balance = acc.balance - _transaction.Amount;

                    db.Entry(acc).State = EntityState.Modified;

                    db.SaveChanges();

                    transaction.transId = Guid.NewGuid();
                    transaction.accId = acc.accId;
                    transaction.timestamp = DateTime.Now;
                    if (_transaction.MerchantID == Guid.Parse("42574452-56bc-4d09-8611-cdd0643ec11a"))
                    {
                        transaction.merchant = "Price Guru";
                    }
                    transaction.amount = _transaction.Amount;
                    transaction.postbalance = acc.balance;
                    transaction.priobalance = postB;

                    if (!ModelState.IsValid)
                    {
                        return BadRequest(ModelState);
                    }

                    db.Transactions.Add(transaction);

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        if (TransactionExists(transaction.transId))
                        {
                            return Conflict();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return CreatedAtRoute("DefaultApi", new { id = transaction.transId }, transaction);
                }
            }

            return NotFound();
        }

        // DELETE: api/TransactionsApi/5
        [ResponseType(typeof(Transaction))]
        public IHttpActionResult DeleteTransaction(Guid id)
        {
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return NotFound();
            }

            db.Transactions.Remove(transaction);
            db.SaveChanges();

            return Ok(transaction);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TransactionExists(Guid id)
        {
            return db.Transactions.Count(e => e.transId == id) > 0;
        }
    }
}